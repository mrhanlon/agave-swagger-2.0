# Agave API Swagger 2.0 Spec

This is a [Swagger API][0] Spec according to the [Swagger 2.0 Schema][1]. It was
created from the Agave API Swagger 1.2 Spec. That spec, downloaded on
2015-Apr-15 is located in the [agave_1_2][2] directory of this repo. Using the
[swagger-converter][3] tool, plus some manual tuning afterward using the [Swagger Editor][4], I produced the [agave_2_0.json][5] specification file.

## Conversion

You can convert the 1.2 spec files into a 2.0 spec file with the command:

```bash
$ npm run convert > spec.json
```

At this time, the produced 2.0 spec file does not validate. It requires manual
tuning.

## Validation

You can validate the spec with the command:

```bash
$ npm test
```

This uses [swagger-tools][6] to validate the spec file (agave_2_0.json).





[0]: http://swagger.io/
[1]: https://github.com/swagger-api/swagger-spec/tree/master/schemas/v2.0
[2]: agave_1_2
[3]: https://www.npmjs.com/package/swagger-converter
[4]: http://editor.swagger.io/
[5]: agave_2_0.json
[6]: https://www.npmjs.com/package/swagger-tools
