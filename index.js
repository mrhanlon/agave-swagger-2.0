var convert = require('swagger-converter');
var fs = require('fs');
var resourceListing = JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/index.json').toString());
var apiDeclarations = [
//  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/adama').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/apps').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/auth').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/clients').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/files').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/jobs').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/meta').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/monitors').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/notifications').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/postits').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/profiles').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/systems').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/track').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/transfers').toString()),
  JSON.parse(fs.readFileSync(__dirname + '/agave_1_2/transforms').toString())
];

var swagger2Document = convert(resourceListing, apiDeclarations);

console.log(JSON.stringify(swagger2Document, null, 2));
